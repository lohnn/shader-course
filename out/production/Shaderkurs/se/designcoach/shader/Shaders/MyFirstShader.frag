#version 430
struct Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

//in Material material;
in vec2 vUV;
in vec3 vNormal;
in vec3 vColor;
in vec3 vView;
layout(binding = 0) uniform sampler2D diffuse; //0 is diffuse texture
layout(binding = 1) uniform sampler2D normalMap;

//Lights
/////////Ambient
//uniform vec3 source_ambient_color = vec3(1.0,1.0,1.0);
layout(location = 2) uniform vec3 source_ambient_color;
/////////Directional light (sun)
const vec3 directional_diffuse_color=vec3(1.,1.,1.);
const vec3 directional_specular_color=vec3(1.,1.,1.);
const vec3 directional_direction=vec3(0.,0.,1.);
/////////Point light
const vec3 point_diffuse_color=vec3(1.,1.,1.);
const vec3 point_specular_color=vec3(1.,0.,1.);
const vec3 point_direction=vec3(0.,0.,1.);
/////////

//Material params
const vec3 mat_ambient_color=vec3(0.3,0.3,0.3);
const vec3 mat_diffuse_color=vec3(1.,1.,1.);
const vec3 mat_specular_color=vec3(1.,1.,1.);
const float mat_shininess=10.;
//////////////////

//Instead of gl_FragColor
layout(location = 0) out vec4 secColor;
void main(void) {
    vec3 color = vec3(texture2D(diffuse, vUV));
    vec3 I_ambient = source_ambient_color * mat_ambient_color;
    vec3 I_diffuse = directional_diffuse_color * mat_diffuse_color * max(0.0, dot(vNormal, directional_direction));
    vec3 V = normalize(vView);
    vec3 R = reflect(directional_direction, vNormal);

    vec3 I_specular = directional_specular_color* mat_specular_color * pow(max(dot(R, V), 0.0), mat_shininess);

    vec3 I = I_ambient;// + I_diffuse + I_specular;
    secColor = vec4(I * color, 1.0);

    //secColor = vec4(source_ambient_color, 1.0);
}