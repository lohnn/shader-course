package se.designcoach.shader;

import com.google.gson.Gson;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import se.designcoach.shader.Geometry.Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

/**
 * Created by lohnn on 2014-03-05.
 */

public class ModelLoader {
    public static Texture loadTexture(URL url, GL4 gl) {
        Texture text = null;
        try {
            File f = new File(url.getPath());
            text = TextureIO.newTexture(f, false);
            text.setTexParameteri(gl, GL4.GL_TEXTURE_MAG_FILTER, GL4.GL_NEAREST);
            text.setTexParameteri(gl, GL4.GL_TEXTURE_MIN_FILTER, GL4.GL_NEAREST);
        } catch (IOException e) {
            System.err.println("Couldn't load diffuseTexture");
            e.printStackTrace();
            System.exit(1);
        }
        return text;
    }

    public Model loadModelOBJ(String file){
        URL url = getClass().getResource(file);
        return OBJParser.parseOBJ(url);
    }

    public Model loadModelJSON(String file) {
        try {
            URL url = getClass().getResource(file);
            File f = new File(url.getPath());
            Model model = new Gson().fromJson(new FileReader(f), Model.class);
            return model;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }
}
