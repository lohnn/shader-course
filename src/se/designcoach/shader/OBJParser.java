package se.designcoach.shader;

import se.designcoach.shader.Geometry.Model;
import se.designcoach.shader.jme3.math.Vector2f;
import se.designcoach.shader.jme3.math.Vector3f;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by temp for gaming on 2015-03-01.
 */
public class OBJParser {
    private static class FaceIndices {
        public FaceIndex[] indices = new FaceIndex[3];

        public FaceIndices(FaceIndex fi1, FaceIndex fi2, FaceIndex fi3) {
            this.indices[0] = fi1;
            this.indices[1] = fi2;
            this.indices[2] = fi3;
        }
    }

    private static class FaceIndex {
        public int vertex, texCoord, normal;

        public FaceIndex(int vertex, int texCoord, int normal) {
            this.vertex = vertex;
            this.texCoord = texCoord;
            this.normal = normal;
        }
    }

    /**
     * Parses an .OBJ-file and loads a model from it.
     * @param url
     * @return
     */
    public static Model parseOBJ(URL url) {
        try {
            String name = "";
            ArrayList<Float> vertices = new ArrayList<>();
            ArrayList<Float> normals = new ArrayList<>();
            ArrayList<Float> texCoords = new ArrayList<>();
            ArrayList<FaceIndices> indices = new ArrayList<>();

            Files.lines(Paths.get(url.toURI())).forEach(s ->
            {
                String[] array = s.split(" ", -1);
                switch (array[0]) { //Vertices
                    case "v":
                        vertices.add(Float.parseFloat(array[1]));
                        vertices.add(Float.parseFloat(array[2]));
                        vertices.add(Float.parseFloat(array[3]));
                        break;
                    case "vt":
                        texCoords.add(Float.parseFloat(array[1]));
                        texCoords.add(Float.parseFloat(array[2]));
                        break;
                    case "vn":
                        normals.add(Float.parseFloat(array[1]));
                        normals.add(Float.parseFloat(array[2]));
                        normals.add(Float.parseFloat(array[3]));
                        break;
                    case "f":
                        indices.add(new FaceIndices(
                                getIndex(array[1]),
                                getIndex(array[2]),
                                getIndex(array[3])));
                        break;
                }
            });

            int[] indicesList = new int[indices.size() * 3];
            ArrayList<Float> tempList = new ArrayList<>();

            for (int i = 0; i < indices.size(); ++i) {
                indicesList[3 * i] = 3 * i;
                indicesList[3 * i + 1] = 3 * i + 1;
                indicesList[3 * i + 2] = 3 * i + 2;

                ////////////////////////////////////////
                //Vertex
                tempList.add(vertices.get(3 * indices.get(i).indices[0].vertex));
                tempList.add(vertices.get(3 * indices.get(i).indices[0].vertex + 1));
                tempList.add(vertices.get(3 * indices.get(i).indices[0].vertex + 2));

                //Texture Coordinate
                tempList.add(texCoords.get(2 * indices.get(i).indices[0].texCoord));
                tempList.add(texCoords.get(2 * indices.get(i).indices[0].texCoord + 1));

                //Normal
                tempList.add(normals.get(3 * indices.get(i).indices[0].normal));
                tempList.add(normals.get(3 * indices.get(i).indices[0].normal) + 1);
                tempList.add(normals.get(3 * indices.get(i).indices[0].normal) + 2);

                ////////////////////////////////////////
                //Vertex
                tempList.add(vertices.get(3 * indices.get(i).indices[1].vertex));
                tempList.add(vertices.get(3 * indices.get(i).indices[1].vertex + 1));
                tempList.add(vertices.get(3 * indices.get(i).indices[1].vertex + 2));

                //Texture Coordinate
                tempList.add(texCoords.get(2 * indices.get(i).indices[1].texCoord));
                tempList.add(texCoords.get(2 * indices.get(i).indices[1].texCoord + 1));

                //Normal
                tempList.add(normals.get(3 * indices.get(i).indices[1].normal));
                tempList.add(normals.get(3 * indices.get(i).indices[1].normal) + 1);
                tempList.add(normals.get(3 * indices.get(i).indices[1].normal) + 2);

                ////////////////////////////////////////
                //Vertex
                tempList.add(vertices.get(3 * indices.get(i).indices[2].vertex));
                tempList.add(vertices.get(3 * indices.get(i).indices[2].vertex + 1));
                tempList.add(vertices.get(3 * indices.get(i).indices[2].vertex + 2));

                //Texture Coordinate
                tempList.add(texCoords.get(2 * indices.get(i).indices[2].texCoord));
                tempList.add(texCoords.get(2 * indices.get(i).indices[2].texCoord + 1));

                //Normal
                tempList.add(normals.get(3 * indices.get(i).indices[2].normal));
                tempList.add(normals.get(3 * indices.get(i).indices[2].normal) + 1);
                tempList.add(normals.get(3 * indices.get(i).indices[2].normal) + 2);
            }

            float[] objectDataList = new float[tempList.size()];
            for (int j = 0; j < tempList.size(); ++j) {
                objectDataList[j] = tempList.get(j);
            }

            return new Model(name, objectDataList, indicesList);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static FaceIndex getIndex(String value) {
        if (value.contains("/")) {
            String[] array = value.split("/", -1);
            return new FaceIndex(Integer.parseInt(array[0]) - 1,
                    Integer.parseInt(array[1]) - 1,
                    Integer.parseInt(array[2]) - 1);
        }
        throw new RuntimeException("OBJ-file needs to contain indices for vertex, texture coordinate and normals");
    }
}