package se.designcoach.shader.Geometry;

import se.designcoach.shader.jme3.math.Matrix4f;
import se.designcoach.shader.jme3.math.Vector3f;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Created by lohnn on 2/18/14.
 */
public class Model {
    private String name;
    private float[] objectData = new float[0];
    private int[] indices = new int[0];

    private Vector3f translatedVector = new Vector3f(Vector3f.ZERO);

    private FloatBuffer objectDataBuffer;
    private IntBuffer indicesBuffer;
    private Matrix4f modelMatrix = new Matrix4f();

    private boolean isAssembled = true;

    /**
     * Create the model
     *
     * @param name       Name of the model
     * @param objectData Object data: (vertex * 3, texture coordinate * 2, normal * 3)
     * @param indices    Indices for when vertices are not in order (currently not supported by shader program)
     */
    public Model(String name, float[] objectData, int[] indices) {
        this.name = name;
        this.objectData = objectData;
        this.indices = indices;
        objectDataBuffer = FloatBuffer.wrap(getObjectData());
        indicesBuffer = IntBuffer.wrap(getIndices());
    }

    public void setTranslatedVector(Vector3f translatedVector) {
        this.translatedVector = translatedVector;
    }

    private float animPosition = 0;

    /**
     * Assembles and disassembles the model, moves all vertices in
     * pre set direction until at end, when at end return true.
     * Run every frame until true is returned.
     *
     * @param deltaTime Time of the frame for constant movement
     * @return False until the model is at end position, then true
     */
    public boolean assemble(float deltaTime) {
        animPosition += deltaTime;
        animPosition = (animPosition > 1) ? 1 : animPosition;

        if (isAssembled) {
            //(end-start) * % + start
            Vector3f temp = (translatedVector.subtract(Vector3f.ZERO)).mult(animPosition).add(Vector3f.ZERO);
            modelMatrix.setTranslation(temp);
        } else {
            Vector3f temp = (Vector3f.ZERO.subtract(translatedVector)).mult(animPosition).add(translatedVector);
            modelMatrix.setTranslation(temp);
        }

        if (animPosition >= 1) {
            isAssembled = !isAssembled;
            animPosition = 0;
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public float[] getObjectData() {
        return objectData;
    }

    public int[] getIndices() {
        return indices;
    }

    @Override
    public String toString() {
        return String.format("name:%s,objectData:%s,indices:%s", name, objectData, indices);
    }

    public FloatBuffer getObjectDataBuffer() {
        return objectDataBuffer;
    }

    public IntBuffer getIndicesBuffer() {
        return indicesBuffer;
    }

    public Matrix4f getModelMatrix() {
        return modelMatrix;
    }

    //Vertex positions
    //Normals
    //Texture positions

    //Texture
    //Normal map texture

    //geometryIndicesData
}
