package se.designcoach.shader;

import se.designcoach.shader.jme3.math.Vector3f;

/**
 * Created by jl222xa on 2014-02-05.
 */
public class Triangle {
    private final float[] vertices;

    public float[] array() {
        return vertices;
    }

    public Triangle(Vector3f vert1, Vector3f vert2, Vector3f vert3) {
        vertices = new float[]{
                vert1.x, vert1.y, vert1.z,
                vert2.x, vert2.y, vert2.z,
                vert3.x, vert3.y, vert3.z
        };
    }

    float[] concat(float[] A, float[] B, float[] C) {
        int aLen = A.length;
        int bLen = B.length;
        int cLen = C.length;
        float[] D = new float[aLen + bLen + cLen];
        System.arraycopy(A, 0, D, 0, aLen);
        System.arraycopy(B, 0, D, aLen, bLen);
        System.arraycopy(C, 0, D, aLen + bLen, cLen);
        return D;
    }
}