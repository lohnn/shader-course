package se.designcoach.shader;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL4;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

/**
 * Created by lohnn on 2/4/14.
 */
public class ShaderControl {
    private int shaderProgram;
    public String[] vsrc;
    public String[] fsrc;

    //This will attach the shaders
    public void init(GL4 gl) {
        try {
            attachShaders(gl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads the shaders
     * In this example we assume that the shader is a file located in the applications JAR file.
     *
     * @param name
     * @return
     */
    public String[] loadShader(String name) {
        StringBuilder sb = new StringBuilder();
        try {
            InputStream is = getClass().getResourceAsStream(name);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*System.out.println("Shader is " + sb.toString());*/
        return new String[]{sb.toString()};
    }

    /**
     * This compiles and loads the shader to the video card.
     * If there is a problem with the source the program will exit at this point.
     *
     * @param gl
     */
    private void attachShaders(GL4 gl) {
        int vertexShaderProgram = gl.glCreateShader(GL2.GL_VERTEX_SHADER);
        int fragmentShaderProgram = gl.glCreateShader(GL2.GL_FRAGMENT_SHADER);
        gl.glShaderSource(vertexShaderProgram, 1, vsrc, null, 0);
        gl.glCompileShader(vertexShaderProgram);

        IntBuffer intBuffer1 = IntBuffer.allocate(1);
        gl.glGetShaderiv(vertexShaderProgram, GL2.GL_COMPILE_STATUS, intBuffer1);
        if (intBuffer1.get(0) != 1) {
            gl.glGetShaderiv(vertexShaderProgram, GL2.GL_INFO_LOG_LENGTH, intBuffer1);
            int size = intBuffer1.get(0);

            if (size > 0) {
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetShaderInfoLog(vertexShaderProgram, size, intBuffer1, byteBuffer);

                System.err.println("Vertexshader compile error: ");
                for (byte b : byteBuffer.array()) {
                    System.err.print((char) b);
                }
            } else {
                System.out.println("Unknown error");
            }
            System.exit(1);
        }


        gl.glShaderSource(fragmentShaderProgram, 1, fsrc, null, 0);
        gl.glCompileShader(fragmentShaderProgram);

        gl.glGetShaderiv(fragmentShaderProgram, GL2.GL_COMPILE_STATUS, intBuffer1);
        if (intBuffer1.get(0) != 1) {
            gl.glGetShaderiv(fragmentShaderProgram, GL2.GL_INFO_LOG_LENGTH, intBuffer1);
            int size = intBuffer1.get(0);

            if (size > 0) {
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetShaderInfoLog(fragmentShaderProgram, size, intBuffer1, byteBuffer);

                System.err.println("Fragmentshader compile error: ");
                for (byte b : byteBuffer.array()) {
                    System.err.print((char) b);
                }
            } else {
                System.out.println("Unknown error");
            }
            System.exit(1);
        }

        shaderProgram = gl.glCreateProgram();

        gl.glAttachShader(shaderProgram, vertexShaderProgram);
        gl.glAttachShader(shaderProgram, fragmentShaderProgram);
        gl.glLinkProgram(shaderProgram);
        gl.glValidateProgram(shaderProgram);
        //intBuffer = IntBuffer.allocate(1);
        IntBuffer intBuffer = IntBuffer.allocate(1);
        gl.glGetProgramiv(shaderProgram, GL2.GL_LINK_STATUS, intBuffer);

        if (intBuffer.get(0) != 1) {
            gl.glGetProgramiv(shaderProgram, GL2.GL_INFO_LOG_LENGTH, intBuffer);
            int size = intBuffer.get(0);

            if (size > 0) {
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetProgramInfoLog(shaderProgram, size, intBuffer, byteBuffer);
                System.err.println("Shader link error: ");
                for (byte b : byteBuffer.array()) {
                    System.err.print((char) b);
                }
            } else {
                System.out.println("Unknown");
            }
            System.exit(1);
        }
    }

    /**
     * This function is called when you want to activate the shader.
     * Once activated, it will be applied to anything that you can draw from here on
     * until you call the dontUseShader(GL2) function.
     *
     * @param gl
     * @return
     */
    public int useShader(GL4 gl) {
        gl.glUseProgram(shaderProgram);
        return shaderProgram;
    }

    /**
     * When you have finished drawing everything that you want using the shaders,
     * call this to stop further shader interactions.
     *
     * @param gl
     */
    public void dontUseShader(GL4 gl) {
        gl.glUseProgram(0);
    }
}
