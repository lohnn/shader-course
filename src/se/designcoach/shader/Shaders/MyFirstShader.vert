#version 430
layout(location = 0) in vec4 position;
layout(location = 1) in vec2 uv; //UV coordinate a.k.a texture coordinate
layout(location = 2) in vec3 normal; //normal
layout(location = 10) uniform mat4 Pmatrix;
layout(location = 11) uniform mat4 Mmatrix;
layout(location = 12) uniform mat4 Vmatrix;
layout(location = 13) uniform mat4 Mmatrix2;

//layout(location = 15) in vec3 color;

//attribute vec4 position; //the position of the point
//attribute vec3 color; //the color of the point

out vec3 vColor;
out vec2 vUV;
out vec3 vNormal;
out vec3 vView;

void main(void) { //pre-built function
    gl_Position = Pmatrix * Vmatrix * Mmatrix * Mmatrix2 * position;// * matrix; //0. is the z, and 1 is w
    vUV = uv;
    vNormal = vec3(Mmatrix*vec4(normal, 0.0));
    vView = vec3(Vmatrix * Mmatrix * position);
    //vColor = position.xyz;
}