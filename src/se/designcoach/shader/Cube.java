package se.designcoach.shader;

import se.designcoach.shader.jme3.math.Vector3f;

/**
 * Created by lohnn on 2014-02-07.
 */
public class Cube {
    private float[] points,
            texCoord;
    private int[] geometryIndicesData;

    public Cube(float width, float height, float depth) {
        Vector3f[] v = computeVertices(width, height, depth);

        points = new float[]{
                //Front
                v[4].x, v[4].y, v[4].z,
                v[5].x, v[5].y, v[5].z,
                v[6].x, v[6].y, v[6].z,
                v[7].x, v[7].y, v[7].z,

                //Right
                v[3].x, v[3].y, v[3].z,
                v[7].x, v[7].y, v[7].z,
                v[5].x, v[5].y, v[5].z,
                v[1].x, v[1].y, v[1].z,

                //Left
                v[2].x, v[2].y, v[2].z,
                v[0].x, v[0].y, v[0].z,
                v[4].x, v[4].y, v[4].z,
                v[6].x, v[6].y, v[6].z,

                //Back
                v[1].x, v[1].y, v[1].z,
                v[0].x, v[0].y, v[0].z,
                v[3].x, v[3].y, v[3].z,
                v[2].x, v[2].y, v[2].z,

                //Top
                v[0].x, v[0].y, v[0].z,
                v[5].x, v[5].y, v[5].z,
                v[4].x, v[4].y, v[4].z,
                v[1].x, v[1].y, v[1].z,

                //Bottom
                v[2].x, v[2].y, v[2].z,
                v[6].x, v[6].y, v[6].z,
                v[7].x, v[7].y, v[7].z,
                v[3].x, v[3].y, v[3].z,
        };


        geometryIndicesData = new int[]{
                0, 1, 2, 1, 3, 2, //Front
                4, 5, 6, 7, 4, 6, //Right
                8, 9, 10, 10, 11, 8, //Left
                12, 13, 14, 13, 15, 14, //Back
                16, 17, 18, 17, 16, 19, //Top
                20, 21, 22, 20, 22, 23, //Bottom
        };

        texCoord = new float[]{
                //Front
                1, 1,
                0, 1,
                1, 0,
                0, 0,

                //Right
                0, 0,
                1, 0,
                1, 1,
                0, 1,

                //Left
                0, 0,
                0, 1,
                1, 1,
                1, 0,

                //Back
                1, 1,
                0, 1,
                1, 0,
                0, 0,

                //Top
                0, 0,
                1, 1,
                0, 1,
                1, 0,

                //Bottom
                1, 1,
                1, 0,
                0, 0,
                0, 1,
        };
    }

    private Vector3f[] computeVertices(float width, float height, float depth) {
        return new Vector3f[]{
                new Vector3f(-width / 2, height / 2, depth / 2),    //[0] Back top left
                new Vector3f(width / 2, height / 2, depth / 2),     //[1] Back top right
                new Vector3f(-width / 2, -height / 2, depth / 2),   //[2] Back bottom left
                new Vector3f(width / 2, -height / 2, depth / 2),    //[3] Back bottom right

                new Vector3f(-width / 2, height / 2, -depth / 2),   //[4] Front top left
                new Vector3f(width / 2, height / 2, -depth / 2),    //[5] Front top right
                new Vector3f(-width / 2, -height / 2, -depth / 2),  //[6] Front bottom left
                new Vector3f(width / 2, -height / 2, -depth / 2),   //[7] Front bottom right
        };
    }

    public int[] indices() {
        return geometryIndicesData;
    }

    public float[] array() {
        return points;
    }

    public float[] getTexCoord() {
        return texCoord;
    }
}