package se.designcoach.shader.ShaderObjects;

import se.designcoach.shader.jme3.math.Vector3f;

/**
 * Created by jl222xa on 2014-06-05.
 */
public class Material {
    private Vector3f ambient,
            diffuse,
            specular;
    private float shininess;

    public Material(Vector3f ambient, Vector3f diffuse, Vector3f specular, float shininess) {
        setAmbient(ambient);
        setDiffuse(diffuse);
        setSpecular(specular);
        setShininess(shininess);
    }

    public Vector3f getAmbient() {
        return ambient;
    }

    public void setAmbient(Vector3f ambient) {
        this.ambient = ambient;
    }

    public Vector3f getDiffuse() {
        return diffuse;
    }

    public void setDiffuse(Vector3f diffuse) {
        this.diffuse = diffuse;
    }

    public Vector3f getSpecular() {
        return specular;
    }

    public void setSpecular(Vector3f specular) {
        this.specular = specular;
    }

    public float getShininess() {
        return shininess;
    }

    public void setShininess(float shininess) {
        this.shininess = shininess;
    }
}
