package se.designcoach.shader;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by lohnn on 2014-02-14.
 */
public class Main {
    public static void main(String[] args) {
        GLProfile glp = GLProfile.getDefault();
        GLCapabilities caps = new GLCapabilities(glp);
        GLCanvas canvas = new GLCanvas(caps);
        SimpleScene scene = new SimpleScene();

        Frame frame = new Frame("AWT Window Test");
        frame.setSize(SimpleScene.screenWidth, SimpleScene.screenHeight);
        frame.add(canvas);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.addKeyListener(scene);

        // by default, an AWT Frame doesn't do anything when you click
        // the close button; this bit of code will terminate the program when
        // the window is asked to close
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        canvas.addGLEventListener(scene);
        canvas.addKeyListener(scene);
        canvas.addMouseListener(scene);
        canvas.addMouseMotionListener(scene);
        canvas.addMouseWheelListener(scene);

        FPSAnimator animator = new FPSAnimator(canvas, 60);
        animator.start();
    }
}
