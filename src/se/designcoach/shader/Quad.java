package se.designcoach.shader;

import se.designcoach.shader.jme3.math.ColorRGBA;
import se.designcoach.shader.jme3.math.Vector3f;

/**
 * Created by jl222xa on 2014-02-06.
 */
public class Quad {
    private Triangle[] triangles = new Triangle[2];
    private float[] colorArray = {
            0, 0, 1, //corner one
            0, 1, 0, //corner two
            1, 0, 0, //corner three

            0, 1, 0, //corner two
            1, 1, 0, //corner four
            1, 0, 0}; //corner three

    public Quad(Vector3f upperLeft, Vector3f bottomRight) {
        triangles[0] = new Triangle(
                upperLeft,
                new Vector3f(upperLeft.x, bottomRight.y, upperLeft.z),
                new Vector3f(bottomRight.x, upperLeft.y, bottomRight.z));
        triangles[1] = new Triangle(
                new Vector3f(upperLeft.x, bottomRight.y, upperLeft.z),
                bottomRight,
                new Vector3f(bottomRight.x, upperLeft.y, bottomRight.z));
    }

    public float[] array() {
        return concat(triangles[0].array(), triangles[1].array());
    }

    public float[] getColorArray() {
        return colorArray;
    }

    float[] concat(float[] A, float[] B) {
        int aLen = A.length;
        int bLen = B.length;
        float[] D = new float[aLen + bLen];
        System.arraycopy(A, 0, D, 0, aLen);
        System.arraycopy(B, 0, D, aLen, bLen);
        return D;
    }

    public void setCornerColors(ColorRGBA one, ColorRGBA two, ColorRGBA three, ColorRGBA four) {
        colorArray[0] = one.r;
        colorArray[1] = one.g;
        colorArray[2] = one.b;

        colorArray[3] = two.r;
        colorArray[4] = two.g;
        colorArray[5] = two.b;

        colorArray[6] = three.r;
        colorArray[7] = three.g;
        colorArray[8] = three.b;

        colorArray[9] = two.r;
        colorArray[10] = two.g;
        colorArray[11] = two.b;

        colorArray[12] = four.r;
        colorArray[13] = four.g;
        colorArray[14] = four.b;

        colorArray[15] = three.r;
        colorArray[16] = three.g;
        colorArray[17] = three.b;
    }
}
