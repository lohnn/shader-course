package se.designcoach.shader.Statics;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Created by lohnn on 2014-02-14.
 */
public class InputButtons {
    public static final int
            ZOOM_IN = KeyEvent.VK_ADD,
            ZOOM_OUT = KeyEvent.VK_SUBTRACT,
            MOVE_LFET = KeyEvent.VK_A,
            MOVE_RIGHT = KeyEvent.VK_D,
            MOVE_UP = KeyEvent.VK_W,
            MOVE_DOWN = KeyEvent.VK_S,
            DEBUG_MESSSAGE = KeyEvent.VK_P,
            PAUSE_ROTATION = KeyEvent.VK_SPACE,
            MOUSE_DRAG = MouseEvent.BUTTON1,
            ASSEMBLE_1 = KeyEvent.VK_1,
            ASSEMBLE_2 = KeyEvent.VK_2,
            ASSEMBLE_3 = KeyEvent.VK_3,
            MODE_NONE = KeyEvent.VK_Z,
            MODE_ANAGLYPH = KeyEvent.VK_X,
            MODE_STEREO = KeyEvent.VK_C;
}
