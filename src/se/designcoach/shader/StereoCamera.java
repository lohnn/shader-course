package se.designcoach.shader;


import com.jogamp.opengl.GL4;
import se.designcoach.shader.jme3.math.Matrix4f;

/**
 * Created by lohnn on 2015-04-23.
 */
public class StereoCamera {
    double eyeSeparation;

    public StereoCamera(double eyeSeparation) {
        this.eyeSeparation = eyeSeparation;
    }

    public void moveLeftEye(GL4 gl, Matrix4f viewMatrix) {
        this.moveLeftEye(gl, viewMatrix, 1);
    }

    public void moveRightEye(GL4 gl, Matrix4f viewMatrix) {
        this.moveRightEye(gl, viewMatrix, 1);
    }

    public void moveLeftEye(GL4 gl, Matrix4f viewMatrix, float scaleX) {
        Matrix4f tempMatrix = viewMatrix.clone();
        tempMatrix.translateLocal((float) (eyeSeparation / 2), 0, 0);
        tempMatrix.setScale(scaleX, 1, 1);
        gl.glUniformMatrix4fv(12, 1, false, tempMatrix.toArray(), 0);       //Projection/cam matrix
    }

    public void moveRightEye(GL4 gl, Matrix4f viewMatrix, float scaleX) {
        Matrix4f tempMatrix = viewMatrix.clone();
        tempMatrix.translateLocal((float) (-eyeSeparation / 2), 0, 0);
        tempMatrix.setScale(scaleX, 1, 1);
        gl.glUniformMatrix4fv(12, 1, false, tempMatrix.toArray(), 0);       //Projection/cam matrix
    }
}
