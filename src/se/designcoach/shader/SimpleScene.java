package se.designcoach.shader;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import se.designcoach.shader.Geometry.Model;
import se.designcoach.shader.Statics.InputButtons;
import se.designcoach.shader.Statics.Settings;
import se.designcoach.shader.jme3.math.FastMath;
import se.designcoach.shader.jme3.math.Matrix4f;
import se.designcoach.shader.jme3.math.Quaternion;
import se.designcoach.shader.jme3.math.Vector3f;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//import javax.media.opengl.GL;
//import javax.media.opengl.GL4;
//import javax.media.opengl.GLAutoDrawable;
//import javax.media.opengl.GLEventListener;

public class SimpleScene implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener, GLEventListener {
    public static int screenWidth = 600, screenHeight = 600;
    private long time = 0;
    float deltaTime = 0;
    private GL4 gl;
    private int program;
    ShaderControl shader;

    //Models
    Model guitarBase;
    Model guitarTop;
    Model guitarBottom;
    Model guitarNeck;
    int guitarObjectDataSize;
    int guitarIndicesSize;

    ArrayList<Model> guitar = new ArrayList<>();
    Map<Model, Integer> modelDataOffset = new HashMap<>();
    Map<Model, Integer> modelIndexOffset = new HashMap<>();
    ArrayList<Model> assembling = new ArrayList<>();

    Matrix4f modelMatrix = new Matrix4f();
    Matrix4f projectionMatrix;
    Matrix4f viewMatrix = new Matrix4f();
    StereoCamera cam = new StereoCamera(3.5f);       // Eye Separation
    Map<DrawMode.Mode, Runnable> renderTypes = new HashMap<>();

    IntBuffer buffers = IntBuffer.allocate(4);
    IntBuffer vertexArray;
    Texture diffuseTexture, normalMapTexture;

    FloatBuffer ambient_color_val = FloatBuffer.wrap(new float[]{1f, 1f, 1f});
//    Material dragonMat = new Material(new Vector3f(.3f, .3f, .3f), new Vector3f(1, 1, 1), new Vector3f(1, 1, 1), 10f);

    //http://www.lighthouse3d.com/tutorials/glsl-core-tutorial/glsl-core-tutorial-uniform-variables/
    //http://stackoverflow.com/questions/17630753/jogl-opengl-vbo-how-to-render-vertices
    //http://www3.ntu.edu.sg/home/ehchua/programming/opengl/JOGL4.0.html
    //http://www.webglacademy.com/
    //http://www.vis.uni-stuttgart.de/glsldevil/
    //https://sites.google.com/site/justinscsstuff/rendering-methods

    //http://www.felixgers.de/teaching/jogl/index.html

    public Texture loadTexture(String file) {
        Texture text = null;
        try {
            URL url = getClass().getResource(file);
            File f = new File(url.getPath());
            text = TextureIO.newTexture(f, false);
            text.setTexParameteri(gl, GL4.GL_TEXTURE_MAG_FILTER, GL4.GL_NEAREST);
            text.setTexParameteri(gl, GL4.GL_TEXTURE_MIN_FILTER, GL4.GL_NEAREST);
        } catch (IOException e) {
            System.err.println("Couldn't load diffuseTexture");
            e.printStackTrace();
            System.exit(1);
        }
        return text;
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        shader = new ShaderControl();
        System.out.println("GL-version: " + drawable.getGL().glGetString(GL.GL_VERSION));
        gl = drawable.getGL().getGL4();

        ModelLoader modelLoader = new ModelLoader();
        guitar.add(guitarBase = modelLoader.loadModelOBJ("3D-Objects/Guitar_base.obj"));
        guitar.add(guitarTop = modelLoader.loadModelOBJ("3D-Objects/Guitar_top.obj"));
        guitar.add(guitarBottom = modelLoader.loadModelOBJ("3D-Objects/Guitar_bottom.obj"));
        guitar.add(guitarNeck = modelLoader.loadModelOBJ("3D-Objects/Guitar_neck.obj"));

        //Set disasembled vectors
        guitarTop.setTranslatedVector(new Vector3f(0, 1, 0));
        guitarBottom.setTranslatedVector(new Vector3f(0, -1, 0));
        guitarNeck.setTranslatedVector(new Vector3f(1.5f, 0, 0));

        modelDataOffset.put(guitarBase, 0);
        modelDataOffset.put(guitarTop, guitarBase.getObjectData().length);
        modelDataOffset.put(guitarBottom, guitarBase.getObjectData().length +
                guitarTop.getObjectData().length);
        modelDataOffset.put(guitarNeck, guitarBase.getObjectData().length +
                guitarTop.getObjectData().length +
                guitarBottom.getObjectData().length);

        modelIndexOffset.put(guitarBase, 0);
        modelIndexOffset.put(guitarTop, guitarBase.getIndices().length);
        modelIndexOffset.put(guitarBottom, guitarBase.getIndices().length +
                guitarTop.getIndices().length);
        modelIndexOffset.put(guitarNeck, guitarBase.getIndices().length +
                guitarTop.getIndices().length +
                guitarBottom.getIndices().length);

        guitarObjectDataSize = (guitarBase.getObjectData().length +
                guitarTop.getObjectData().length +
                guitarBottom.getObjectData().length +
                guitarNeck.getObjectData().length);

        guitarIndicesSize = (guitarBase.getIndices().length +
                guitarTop.getIndices().length +
                guitarBottom.getIndices().length +
                guitarNeck.getIndices().length);

        vertexArray = IntBuffer.allocate(1);

        time = System.currentTimeMillis();

        //Create program
        program = gl.glCreateProgram();

        shader.vsrc = shader.loadShader("Shaders/MyFirstShader.vert");
        shader.fsrc = shader.loadShader("Shaders/MyFirstShader.frag");
        shader.init(gl);
        gl.glEnable(GL4.GL_CULL_FACE);
        gl.glEnable(GL4.GL_TEXTURE_2D);

        //Create vertex array.
        gl.glGenVertexArrays(1, vertexArray);
        gl.glBindVertexArray(vertexArray.get(0));

        gl.glGenBuffers(4, buffers);

        ////////////////////////////////////////
        //Vertices
        gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, buffers.get(0));
        gl.glBufferData(GL4.GL_ARRAY_BUFFER, 4 * guitarObjectDataSize, FloatBuffer.allocate(0), GL4.GL_STATIC_DRAW);

        gl.glBufferSubData(GL4.GL_ARRAY_BUFFER, 4 * modelDataOffset.get(guitarBase),
                4 * guitarBase.getObjectData().length, guitarBase.getObjectDataBuffer());

        gl.glBufferSubData(GL4.GL_ARRAY_BUFFER, 4 * modelDataOffset.get(guitarBottom),
                4 * guitarBottom.getObjectData().length, guitarBottom.getObjectDataBuffer());

        gl.glBufferSubData(GL4.GL_ARRAY_BUFFER, 4 * modelDataOffset.get(guitarTop),
                4 * guitarTop.getObjectData().length, guitarTop.getObjectDataBuffer());

        gl.glBufferSubData(GL4.GL_ARRAY_BUFFER, 4 * modelDataOffset.get(guitarNeck),
                4 * guitarNeck.getObjectData().length, guitarNeck.getObjectDataBuffer());

        //VertexAttribArray 0 corresponds with location 0 in the vertex shader aka position
        gl.glEnableVertexAttribArray(0);
        gl.glVertexAttribPointer(0, 3, GL4.GL_FLOAT, false, 4 * (3 + 2 + 3), 0);
        ////////////////////////////////////////
        //Texture coordinates
        gl.glEnableVertexAttribArray(1);
        gl.glVertexAttribPointer(1, 2, GL4.GL_FLOAT, false, 4 * (3 + 2 + 3), 4 * 3);
        ////////////////////////////////////////
        //Normals
        gl.glEnableVertexAttribArray(2);
        gl.glVertexAttribPointer(2, 3, GL4.GL_FLOAT, false, 4 * (3 + 2 + 3), 4 * (2 + 3));
        ////////////////////////////////////////
        //Indices
//        gl.glBindBuffer(GL4.GL_ELEMENT_ARRAY_BUFFER, buffers.get(2));
//        gl.glBufferData(GL4.GL_ELEMENT_ARRAY_BUFFER, 4 * guitarIndicesSize, IntBuffer.allocate(0), GL4.GL_STATIC_DRAW);
//        gl.glBufferSubData(GL4.GL_ARRAY_BUFFER, 0, 4 * guitarBase.getIndices().length, guitarBase.getIndicesBuffer());
//        gl.glBufferSubData(GL4.GL_ARRAY_BUFFER, 4 * guitarBase.getIndices().length,
//                4 * guitarBottom.getIndices().length, guitarBottom.getIndicesBuffer());

        ////////////////////////////////////////

        //Testing material stuff
        diffuseTexture = loadTexture("Textures/wood_texture.jpg");
        normalMapTexture = loadTexture("Textures/brickwork_normal-map.jpg");

        gl.glActiveTexture(GL4.GL_TEXTURE0);
        diffuseTexture.bind(gl);

        gl.glActiveTexture(GL4.GL_TEXTURE1);
        normalMapTexture.bind(gl);
        //Stop testing material stuff

        //Tell the program to use the shader
        shader.useShader(gl);

        //If camera settings changes, this should change as well.
        projectionMatrix = Matrix4f.ProjectionMatrix(40, screenWidth / screenHeight, 1, 100);

        gl.glEnable(GL4.GL_DEPTH_TEST);

        gl.glUniform3fv(2, 1, ambient_color_val);

        modelMatrix.setTranslation(0, -4f, -12.5f);

        Quaternion toRotate = new Quaternion();
        toRotate.fromAngleAxis(FastMath.HALF_PI / 3, Vector3f.UNIT_X);
        Quaternion initRotation = modelMatrix.toRotationQuat();
        modelMatrix.setRotationQuaternion(toRotate.multLocal(initRotation));

        ///////////////////////////////////
        //Predefine the render types
        renderTypes.put(DrawMode.Mode.none, this::renderNormal);
        renderTypes.put(DrawMode.Mode.anaglyph, this::renderAnaglyph);
        renderTypes.put(DrawMode.Mode.stereo, this::renderStereo);
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        gl.glClear(GL4.GL_DEPTH_BUFFER_BIT | GL4.GL_COLOR_BUFFER_BIT);
        update();
        render(drawable);
    }

    private void update() {
        deltaTime = (float) (System.currentTimeMillis() - time) / 1000;
        time = System.currentTimeMillis();

        //Assembles or disassembles all models in the assembling list, if returns true, remove from list
        Iterator<Model> i = assembling.iterator();
        while (i.hasNext()) {
            Model m = i.next(); // must be called before you can call i.remove()
            if (m.assemble(deltaTime))
                i.remove();
        }
//        assembling.stream().filter(m -> m.assemble(deltaTime)).forEach(assembling::remove);

        if (!isDragging && isRotating) {
            rotateModel(FastMath.HALF_PI * deltaTime / 3, Vector3f.UNIT_Y);
            rotateModel(FastMath.HALF_PI * deltaTime / 2 / 3, Vector3f.UNIT_X);
        }
    }

    private void rotateModel(float angle, Vector3f axis) {
        Quaternion toRotate = new Quaternion();
        toRotate.fromAngleAxis(angle, axis);
        Quaternion initRotation = modelMatrix.toRotationQuat();
        modelMatrix.setRotationQuaternion(toRotate.multLocal(initRotation));
    }

    private void render(GLAutoDrawable drawable) {
        //Matrices
        gl.glUniformMatrix4fv(10, 1, false, projectionMatrix.toArray(), 0); //Projection/cam matrix
        gl.glUniformMatrix4fv(11, 1, false, modelMatrix.toArray(), 0);      //Move/model matrix

        renderTypes.get(DrawMode.getMode()).run();
    }

    private void renderNormal() {
        gl.glViewport(0, 0, screenWidth, screenHeight);
        gl.glUniformMatrix4fv(12, 1, false, viewMatrix.toArray(), 0);       //Projection/cam matrix
        renderObjects();
    }

    private void renderAnaglyph() {
        gl.glViewport(0, 0, screenWidth, screenHeight);
        cam.moveLeftEye(gl, viewMatrix);
        gl.glColorMask(true, false, false, false);
        renderObjects();
        gl.glClear(gl.GL_DEPTH_BUFFER_BIT);
        cam.moveRightEye(gl, viewMatrix);
        gl.glColorMask(false, true, true, false);
        renderObjects();
        gl.glColorMask(true, true, true, true);
    }

    private void renderStereo() {
        gl.glViewport(0, 0, screenWidth / 2, screenHeight);
        cam.moveLeftEye(gl, viewMatrix, 2);
        renderObjects();

        gl.glViewport(screenWidth / 2, 0, screenWidth / 2, screenHeight);
        cam.moveRightEye(gl, viewMatrix, 2);
        renderObjects();
    }

    private void renderObjects() {
        for (Model m : guitar) {
            gl.glUniformMatrix4fv(13, 1, false, m.getModelMatrix().toArray(), 0); //Model
            gl.glDrawArrays(GL4.GL_TRIANGLES, modelIndexOffset.get(m), m.getIndices().length);
        }
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {
        gl.glDisableVertexAttribArray(0);
        gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, 0);
        gl.glDeleteBuffers(1, vertexArray);
        shader.dontUseShader(gl);
        if (diffuseTexture != null)
            diffuseTexture.destroy(gl);
    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int i, int i2, int i3, int i4) {

    }


    //Input listeners
    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
    }

    private boolean isRotating = false,
            isDragging = false;

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case InputButtons.ZOOM_IN:
                modelMatrix.translateLocal(0, 0, 1);
                System.out.println(modelMatrix.toString());
                break;
            case InputButtons.ZOOM_OUT:
                modelMatrix.translateLocal(0, 0, -1);
                System.out.println(modelMatrix.toString());
                break;
            case InputButtons.MOVE_LFET:
                modelMatrix.translateLocal(-0.5f, 0, 0);
                break;
            case InputButtons.MOVE_RIGHT:
                modelMatrix.translateLocal(0.5f, 0, 0);
                break;
            case InputButtons.MOVE_UP:
                modelMatrix.translateLocal(0, 0.5f, 0);
                break;
            case InputButtons.MOVE_DOWN:
                modelMatrix.translateLocal(0, -0.5f, 0);
                break;
            case InputButtons.DEBUG_MESSSAGE:
                break;
            case InputButtons.PAUSE_ROTATION:
                isRotating = !isRotating;
                break;
            case InputButtons.ASSEMBLE_1:
                addToAssemblingList(guitarTop);
                break;
            case InputButtons.ASSEMBLE_2:
                addToAssemblingList(guitarBottom);
                break;
            case InputButtons.ASSEMBLE_3:
                addToAssemblingList(guitarNeck);
                break;
            case InputButtons.MODE_NONE:
                DrawMode.setMode(DrawMode.Mode.none);
                break;
            case InputButtons.MODE_ANAGLYPH:
                DrawMode.setMode(DrawMode.Mode.anaglyph);
                break;
            case InputButtons.MODE_STEREO:
                DrawMode.setMode(DrawMode.Mode.stereo);
                break;
            default:
        }
    }

    /**
     * Adds model to assemble list if not already exist
     *
     * @param m
     */
    private void addToAssemblingList(Model m) {
        if (assembling.indexOf(m) == -1)
            assembling.add(m);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == InputButtons.MOUSE_DRAG) {
            isDragging = true;
            lastMousePoint.setLocation(mouseEvent.getPoint());
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == InputButtons.MOUSE_DRAG) {
            isDragging = false;
        }
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        //Rotating the model
        if (isDragging) {
            Point newPoint = mouseEvent.getPoint();
            rotateModel((float) (newPoint.x - lastMousePoint.x) / 100, Vector3f.UNIT_Y); //Rotate around y
            rotateModel((float) (newPoint.y - lastMousePoint.y) / 100, Vector3f.UNIT_X); //Rotate around x
            lastMousePoint.setLocation(newPoint);
        }
    }

    Point lastMousePoint = new Point();

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
        modelMatrix.translateLocal(0, 0, mouseWheelEvent.getWheelRotation() * Settings.INVERTED_SCROLL);
    }
}