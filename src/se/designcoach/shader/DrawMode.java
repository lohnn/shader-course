package se.designcoach.shader;

/**
 * Created by lohnn on 2015-04-24.
 */
public class DrawMode {
    public enum Mode {
        none,
        anaglyph,
        stereo,
    }

    private static Mode mode = Mode.none;

    public static Mode getMode(){
        return mode;
    }

    public static void setMode(Mode mode){
        DrawMode.mode = mode;
    }
}
